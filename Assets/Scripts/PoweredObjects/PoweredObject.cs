﻿using UnityEngine;
using System.Collections;

public class PoweredObject : MonoBehaviour {

	protected bool active = false;

	public virtual void Activate() {active = true;}
	public virtual void Deactivate() {active = false;}

}
