﻿using UnityEngine;
using System.Collections;

public class Saw : PoweredObject {

	Animator anim;
	float timer = 0;
	float maxTime = 0.6f;
	bool respawning = false;

	void Start () {
		anim = GetComponent<Animator> ();
		Activate ();
	}
	
	void Update () {
		if (respawning) {
			timer += Time.deltaTime;
			if (timer >= maxTime){
				respawning = false;
			}
		}

		if (active) {
			anim.SetBool ("Spinning", true);
		} else {
			anim.SetBool ("Spinning", false);
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.tag == "Player") {
			if (!respawning) {
				respawning = true;
				other.gameObject.GetComponent<PlayerMovement>().Respawn ();
			}
		}
	}
}
