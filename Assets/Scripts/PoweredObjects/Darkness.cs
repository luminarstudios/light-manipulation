﻿using UnityEngine;
using System.Collections;

public class Darkness : PoweredObject {

	bool disabled = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (active && !disabled) {
			foreach (Transform t in transform) {
				t.gameObject.SetActive(false);
			}
			disabled = true;
		} else if (!active && disabled) {
			foreach (Transform t in transform) {
				t.gameObject.SetActive(true);
			}
			disabled = false;
		}
	}
}
