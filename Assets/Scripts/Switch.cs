﻿using UnityEngine;
using System.Collections;

public class Switch : Selectable {

	public PoweredObject poweredObject;

	public bool active = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (isSelected) {
			Highlight.On (gameObject);
			if (Input.GetButtonDown ("Interact")) {
				active = !active;
				if (active) {
					this.transform.localScale = new Vector3 (this.transform.localScale.x, -1, this.transform.localScale.z);
					poweredObject.Activate ();
				} else {
					this.transform.localScale = new Vector3 (this.transform.localScale.x, 1, this.transform.localScale.z);
					poweredObject.Deactivate();
				}
			}
		} else {
			Highlight.Off(gameObject);
		}
		isSelected = false;
	}
}
