﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Sensor : MonoBehaviour {

	public LightColors.LightColor sensorColor;
	LightColors.LightColor hitColor = LightColors.LightColor.None;
	bool isHit = false;
	public bool isHitWithAppropriateColor {
		get {
			return ColorsMatch ();
		}
	}

	void Start () {
		GetComponent<SpriteRenderer>().color = LightColors.colorDictionary[sensorColor];
	}
	
	void Update () {

		if (isHit) {
			ColorsMatch ();
		} else {
			hitColor = LightColors.LightColor.None;
		}
		isHit = false;
	}

	public void SensorHit (LightColors.LightColor lightColor){
		hitColor = lightColor;
		isHit = true;
	}

	bool ColorsMatch () {
		if (sensorColor == hitColor) {
			return true;
		} else {
			return false;
		}
	}
}
