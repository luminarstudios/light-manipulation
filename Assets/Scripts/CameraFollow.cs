﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
	public Vector2 margin;
	public Vector2 smoothing;
	private Vector3 _min, _max;
	Transform player;
	BoxCollider2D bounds;

	void Awake () {
		Cursor.visible = false;
		bounds = GameObject.FindGameObjectWithTag ("CameraBounds").GetComponent<BoxCollider2D>();
		_min = bounds.bounds.min;
		_max = bounds.bounds.max;
		player = GameObject.FindGameObjectWithTag("Player").transform;
		if (player != null) {
			Follow ();
		}
	}

	void Start ()
	{

	}

	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit();
		}
	}
	
	void FixedUpdate ()
	{
		if (player != null) {
			Follow ();
		}
	}

	void Follow ()
	{
		float x = transform.position.x;
		float y = transform.position.y;

		if (Mathf.Abs (x - player.position.x) < margin.x) {
			x = Mathf.Lerp (x, player.position.x, smoothing.x * Time.deltaTime);
		}
		if (Mathf.Abs (y - player.position.y) < margin.y) {
			y = Mathf.Lerp (y, player.position.y, smoothing.y * Time.deltaTime);
		}

		if (bounds.gameObject.activeSelf) {
			float cameraHalfWidth = Camera.main.orthographicSize * ((float)Screen.width / Screen.height);

			x = Mathf.Clamp (x, _min.x + cameraHalfWidth, _max.x - cameraHalfWidth);
			y = Mathf.Clamp (y, _min.y + Camera.main.orthographicSize, _max.y - Camera.main.orthographicSize);
		}

		transform.position = new Vector3 (x, y, transform.position.z);
	}
}
