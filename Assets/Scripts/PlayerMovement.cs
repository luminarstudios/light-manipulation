﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	public static int startingAtDoorID = 0;

	public float movementSpeed = 10;
	public float jumpForce = 20;
	public float distance = 1.0f;
	public LayerMask jumpLayers, objectLayers;
	public bool isGrounded = false;
	public bool isTouchingLadder = false;
	public float ladderPosition;
	[Header("Development Position Override")]
	public bool devMode = false;
	public Transform [] checkpoints;

	GameObject closest;
	Rigidbody2D rigid;
	Animator anim;
	bool onLadder = false;

	// PLATORM JUMP BUG FIX
	float jumpTimer = 0;
	float jumpMaxTime = 0.4f;
	bool inAir = false;
	// --------------------

	void Awake () {
		if (!devMode) {
			Vector3 newPos = GameObject.Find ("Door" + startingAtDoorID).transform.position;
			newPos.z = this.transform.position.z;
			this.transform.position = newPos;
		}
		//Camera.main.transform.position = new Vector3 (newPos.x, newPos.y, Camera.main.transform.position.z);
	}

	void Start ()
	{
		LightColors.SetValues ();
		rigid = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
	}
	
	void Update ()
	{
		if (inAir) {
			jumpTimer += Time.deltaTime;
			if (jumpTimer >= jumpMaxTime) {
				jumpTimer = 0;
				inAir = false;
			}
		}

		isGrounded = Physics2D.OverlapCircle (transform.position, distance, jumpLayers) != null ? true : false;

		if (Input.GetAxis ("Horizontal") > 0) {
			transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
		} else if (Input.GetAxis ("Horizontal") < 0) {
			transform.localScale = new Vector3(-Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
		}
		closest = gameObject;
		foreach (Collider2D col in Physics2D.OverlapCircleAll(transform.position, 0.75f, objectLayers)) {
			if ((transform.position - col.transform.position).sqrMagnitude < (transform.position - closest.transform.position).sqrMagnitude || closest == gameObject){
				closest = col.gameObject;
			}
		}
		if (closest.GetComponent<Selectable>() != null){
			closest.GetComponent<Selectable>().isSelected = true;
		}

		if (Input.GetButtonDown ("Jump") && isGrounded) {
			Jump ();
		}
		rigid.gravityScale = onLadder ? 0 : 5;
		if (onLadder) {
			transform.position = new Vector3 (ladderPosition, transform.position.y, transform.position.z);
		}
	}
	
	void FixedUpdate ()
	{
		if (!onLadder) {
			Move (Input.GetAxis ("Horizontal"));
		}
		rigid.velocity = new Vector2 (Mathf.Clamp(rigid.velocity.x, -movementSpeed, movementSpeed), rigid.velocity.y);

		if (isTouchingLadder) {
			if (!onLadder && Mathf.RoundToInt(Input.GetAxis ("Vertical")) != 0 || !onLadder && Input.GetKey(KeyCode.W) || !onLadder && Input.GetKey(KeyCode.S)) {
				onLadder = true;
				rigid.velocity = Vector2.zero;
				rigid.drag = 50;
				//rigid.isKinematic = true;
			}
			if (onLadder && Mathf.RoundToInt(Input.GetAxis ("Vertical")) != 0 || onLadder && Input.GetKey(KeyCode.W) || onLadder && Input.GetKey(KeyCode.S)) {
				transform.position += transform.up * Input.GetAxis ("Vertical") * Time.deltaTime * 5f;
			} else if (onLadder && Input.GetAxis ("Horizontal") != 0){
				transform.position += transform.right * 10 * Time.deltaTime * (Mathf.Abs(Input.GetAxis ("Horizontal")) / Input.GetAxis ("Horizontal"));
				onLadder = false;
				rigid.drag = 0;
				//rigid.isKinematic = false;
			}
		} else {
			rigid.drag = 0;
			onLadder = false;
			//rigid.isKinematic = false;
		}
	}
	
	void Jump ()
	{
		if (isGrounded && !inAir) {
			rigid.AddForce (Vector2.up * jumpForce, ForceMode2D.Impulse);
			inAir = true;
		}
	}
	
	void Move (float direction)
	{
		// NOTE: Use addition to force
		int dir = 0;
		if (direction != 0) {
			dir = (int)(Mathf.Abs (direction) / direction);
		}

		rigid.AddForce (new Vector2(dir * movementSpeed, 0), ForceMode2D.Impulse);
		anim.SetFloat ("Move", Mathf.Abs(rigid.velocity.x / 4.6076f));
	}

	public void Respawn () {
		Vector3 respawnPos = new Vector3();
		for (int i = checkpoints.Length - 1; i < checkpoints.Length; i--) {
			if(transform.position.x > checkpoints[i].position.x) {
				respawnPos = checkpoints[i].position;
				transform.position = new Vector3(respawnPos.x, respawnPos.y, 0);
				break;
			}
		}
	}
}
