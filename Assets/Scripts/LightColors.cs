﻿using UnityEngine;
using System.Collections.Generic;

public class LightColors {

	public static Dictionary <LightColor,Color> colorDictionary;

	public enum LightColor {None, Red, Green, Blue, Yellow, Magenta, Orange}

	public static Color Red () {
		return new Color (0.89804f,0f,0f);
	}

	public static Color Green () {
		return new Color (0f,0.992157f,0.00784f);
	}

	public static Color Blue () {
		return new Color (0.011372f,0.870588f,0.886274f);
	}

	public static Color Orange () {
		return new Color (1f,0.43137f,0.00392f);
	}

	public static void SetValues () {
		colorDictionary = new Dictionary<LightColor, Color>();
		colorDictionary.Add (LightColor.Red, Red ());
		colorDictionary.Add (LightColor.Green, Green ());
		colorDictionary.Add (LightColor.Blue, Blue ());
		colorDictionary.Add (LightColor.Yellow, Color.yellow);
		colorDictionary.Add (LightColor.Magenta, Color.magenta);
		colorDictionary.Add (LightColor.Orange, Orange ());
	}
}
