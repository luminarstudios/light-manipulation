﻿using UnityEngine;
using System.Collections;

public class Platform : PoweredObject
{

	public float jumpOffset = 1.0f;
	public float movementSpeed = 5;
	[Header("X")]
	public float
		amountToMoveOnX = 0;
	[Header("Y")]
	public float
		amountToMoveOnY = 0;
	[Header("")]
	public bool isMoving = false;
	public bool moveOnce = false;
	public bool stopWithoutPower = true;
	Collider2D myColl;
	Transform playerPos;
	float timer = 0;
	float maxTime = 0.5f;
	Vector3 myPosOnStart;
	Vector3 targetPos;
	bool dropping = false;
	bool standingOnThis = false;
	bool movingForward = true;

	void Start ()
	{
		playerPos = GameObject.FindGameObjectWithTag ("Player").transform;
		myColl = GetComponent<Collider2D> ();
		myColl.enabled = false;
		myPosOnStart = transform.parent.localPosition;
		targetPos = new Vector3 (myPosOnStart.x + amountToMoveOnX, myPosOnStart.y + amountToMoveOnY, myPosOnStart.z);
		if (isMoving) {
			Activate ();
		}
	}
	
	void Update ()
	{
		if (!moveOnce) {
			if (active) {
				isMoving = true;
			} else if (!active && stopWithoutPower) {
				isMoving = false;
			}
		} else {
			isMoving = true;
		}

		if (playerPos.position.y >= transform.parent.position.y + jumpOffset) {
			if (!dropping) {
				myColl.enabled = true;
			}
			if (Mathf.Round (Input.GetAxis ("Vertical")) < 0 && standingOnThis) {
				myColl.enabled = false;
				dropping = true;
			}

		} else {
			myColl.enabled = false;
		}

		if (dropping) {
			myColl.enabled = false;
			timer += Time.deltaTime;
			if (timer >= maxTime) {
				timer = 0;
				dropping = false;
			}
		}
	}

	void FixedUpdate ()
	{
		if (isMoving) {
			if (moveOnce) {
				movingForward = active;
				if ((transform.parent.localPosition - targetPos).sqrMagnitude > 0.001f || (transform.parent.localPosition - myPosOnStart).sqrMagnitude > 0.001f) {
					if (movingForward) {
						Move (targetPos);
					} else {
						Move (myPosOnStart);
					}
				}
			} else {
				if ((transform.parent.localPosition - targetPos).sqrMagnitude < 0.001f && movingForward) {
					movingForward = false;
				} else if ((transform.parent.localPosition - myPosOnStart).sqrMagnitude < 0.001f && !movingForward) {
					movingForward = true;
				}
				if (movingForward) {
					Move (targetPos);
				} else {
					Move (myPosOnStart);
				}
			}
		}
	}

	void Move (Vector3 targetPos)
	{
		transform.parent.localPosition = Vector3.MoveTowards (transform.parent.localPosition, targetPos, movementSpeed * Time.deltaTime);
	}

	void OnCollisionEnter2D (Collision2D other)
	{
		if (other.gameObject.tag == "Player") {
			standingOnThis = true;
		}
	}

	void OnCollisionStay2D (Collision2D other)
	{
		if (other.gameObject.tag == "Player") {
			standingOnThis = true;
		}
	}
	
	void OnCollisionExit2D (Collision2D other)
	{
		if (other.gameObject.tag == "Player") {
			standingOnThis = false;
		}
	}

	void OnTriggerStay2D (Collider2D other)
	{
		if (other.gameObject.tag == "Player") {
			other.gameObject.transform.parent = transform.parent;
		}
	}

	void OnTriggerExit2D (Collider2D other)
	{
		if (other.gameObject.tag == "Player") {
			other.gameObject.transform.parent = null;
		}
	}
}
