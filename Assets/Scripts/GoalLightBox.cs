﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GoalLightBox : MonoBehaviour {

	public Sensor [] sensors;
	bool [] allBoxSensors;
	[Header("Power")]
	public PoweredObject poweredObject;

	void Start () {
		allBoxSensors = new bool[sensors.Length];
	}
	
	void Update () {
		for (int i = 0; i < sensors.Length; i++){
			allBoxSensors[i] = sensors[i].GetComponent<Sensor>().isHitWithAppropriateColor ? true : false;
		}
		goalIsReached ();
	}

	public bool goalIsReached () {
		foreach(bool b in allBoxSensors){
			if (!b){
				poweredObject.Deactivate();
				return false;
			}
		}
		poweredObject.Activate ();
		return true;
	}
}