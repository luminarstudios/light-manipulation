﻿using UnityEngine;
using System.Collections;

public class Mirror : Selectable {

	public bool rotatable = false;

	void Start () {
	}
	
	void Update () {
		if (!rotatable)
			return;

		if (isSelected) {
			Highlight.On (gameObject);
			if (Input.GetKey (KeyCode.W)) {
				RotateMiror (60);
			} else if (Input.GetKey (KeyCode.S)) {
				RotateMiror (-60);
			}

			if (Input.GetAxis("RotateMirrorAxis") != 0){
				//RotateMiror (60 * Input.GetAxis ("RotateMirrorAxis"));
			}
			if (Input.GetButton("RotateMirrorLeft")) {
				RotateMiror (60);
			} else if (Input.GetButton("RotateMirrorRight")) {
				RotateMiror (-60);
			}
		} else {
			Highlight.Off (gameObject);
		}
		isSelected = false;
	}

	public void RotateMiror (float rotationAmount) {
		transform.Rotate (0, 0, rotationAmount * Time.deltaTime);
	}
}
