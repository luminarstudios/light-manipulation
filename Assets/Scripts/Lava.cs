﻿using UnityEngine;
using System.Collections;

public class Lava : MonoBehaviour {

	float timer = 0;
	float maxTime = 0.6f;
	bool respawning = false;

	void Start () {}
	
	void Update () {
		if (respawning) {
			timer += Time.deltaTime;
			if (timer >= maxTime){
				respawning = false;
			}
		}
	}

	void OnCollisionEnter2D (Collision2D other) {
		if (other.gameObject.tag == "Player") {
			if (!respawning) {
				respawning = true;
				other.gameObject.GetComponent<PlayerMovement>().Respawn ();
			}
		}
	}
}
