﻿using UnityEngine;
using System.Collections;

public class Ladder : MonoBehaviour {

	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.tag == "Player") {
			other.GetComponent<PlayerMovement>().isTouchingLadder = true;
			other.GetComponent<PlayerMovement>().ladderPosition = transform.position.x;
		}
	}

	void OnTriggerStay2D (Collider2D other) {
		if (other.gameObject.tag == "Player") {
			other.GetComponent<PlayerMovement>().isTouchingLadder = true;
			other.GetComponent<PlayerMovement>().ladderPosition = transform.position.x;
		}
	}

	void OnTriggerExit2D (Collider2D other) {
		if (other.gameObject.tag == "Player") {
			other.GetComponent<PlayerMovement>().isTouchingLadder = false;
		}
	}
}
