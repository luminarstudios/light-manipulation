﻿using UnityEngine;
using System.Collections.Generic;

public class Highlight : MonoBehaviour {

	public GameObject highlightPrefab;
	public static Highlight instance;
	public Dictionary<GameObject, GameObject> highlights = new Dictionary<GameObject, GameObject>();

	void Start () {
		Highlight.instance = this;
	}

	public static void On (GameObject go) {
		if (!Highlight.instance.highlights.ContainsKey (go)) {
			GameObject _go = (GameObject)Instantiate (Highlight.instance.highlightPrefab, go.transform.position + Vector3.forward * 0.1f, Quaternion.identity);
			_go.transform.parent = go.transform;
			Highlight.instance.highlights.Add (go, _go);
		}
	}

	public static void Off (GameObject go) {
		if (Highlight.instance.highlights.ContainsKey (go)) {
			Destroy (Highlight.instance.highlights[go]);
			Highlight.instance.highlights.Remove (go);
		}
	}
}
