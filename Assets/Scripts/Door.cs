﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour
{

	public Sprite open, close;
	public string levelName;
	public int leadingToDoorID = 0;

	SpriteRenderer rend;
	bool isOpen = false;

	void Start ()
	{
		rend = GetComponent<SpriteRenderer> ();
	}
	
	void Update ()
	{
		if (Physics2D.OverlapCircle (transform.position, 1.2f, 1 << LayerMask.NameToLayer("Player"))) {
			isOpen = true;
			if (Input.GetButtonDown ("Interact")) {
				ChangeLevel ();
			}
		} else {
			isOpen = false;
		}

		rend.sprite = isOpen ? open : close;
	}

	void ChangeLevel ()
	{
		PlayerMovement.startingAtDoorID = leadingToDoorID;
		Application.LoadLevel (levelName);
	}
}
