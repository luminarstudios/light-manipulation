﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Lamp : PoweredObject {

	public LayerMask layerMask;
	public LightColors.LightColor lightColor;

	public GameObject lightRayPrefab;
	public bool isOn = false;

	int rayCount = 0;

	List<GameObject> availableLightRays = new List<GameObject>();

	void Start () {
		if (isOn)
			active = true;
	}

	void Update(){

	}
	
	void FixedUpdate () {

		DestroyAllChildren (gameObject);

		if (active) {
			ShootRay (this.transform.position + transform.up * 0.68f, this.transform.up, new RaycastHit2D ());
		}
	}

	void DestroyAllChildren(GameObject go){
		foreach (Transform t in this.transform) {
			t.GetComponent<LineRenderer>().SetVertexCount(0);
			availableLightRays.Add(t.gameObject);
		}

		rayCount = 0;
	}

	void ShootRay(Vector2 position, Vector2 direction, RaycastHit2D oldHit){
		// BREAK AFTER 100 BOUNCES
		if(rayCount >= 10)
			return;

		GameObject lineGO = availableLightRays.Count > 0 ? availableLightRays[0] : null;
		if (lineGO != null) {
			availableLightRays.RemoveAt (0);
		} else {
			lineGO = (GameObject)Instantiate (lightRayPrefab);
		}

		rayCount++;
		lineGO.transform.parent = this.transform;

		LineRenderer line = lineGO.GetComponent<LineRenderer> ();
		line.SetVertexCount (2);
		line.material.color = LightColors.colorDictionary [lightColor];
		
		line.SetPosition (0, position);


		RaycastHit2D[] hits = Physics2D.RaycastAll (position, direction, Mathf.Infinity, layerMask);

		RaycastHit2D hit = Physics2D.Raycast (position, direction, Mathf.Infinity, layerMask);

		if (hits.Length > 1 && hit.collider == oldHit.collider) {
			hit = hits [1];
		}

		if (hit.collider != oldHit.collider && hit.collider != null) {

			if (hit.collider.gameObject.layer == LayerMask.NameToLayer ("Mirror")) {
				line.SetPosition (1, hit.point);
				ShootRay (hit.point, Vector3.Reflect (direction, -hit.normal), hit);
			} else {

				if(hit.collider.gameObject.layer == LayerMask.NameToLayer("LightSensor")){
					hit.collider.GetComponent<Sensor>().SensorHit(lightColor);
				}

				line.SetPosition (1, hit.point);
			}

		} else {
			line.SetPosition (1, position + direction * 100);
		}
	}
}
